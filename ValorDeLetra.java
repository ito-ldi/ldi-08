/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.AsciiABinario;

/**
 *
 * @author Luis
 */
public class ValorDeLetra {
    private ValorDeLetra tipoLetra;
    private String letra;
    private int valor;
    public ValorDeLetra(String letra, int valor){
        this.letra = letra;
        this.valor = valor;
    }

    public ValorDeLetra getTipoLetra() {
        return tipoLetra;
    }

    public void setTipoLetra(ValorDeLetra tipoLetra) {
        this.tipoLetra = tipoLetra;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
}
