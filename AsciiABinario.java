 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.AsciiABinario;
import java.io.*;
import java.util.StringTokenizer;

/**
 *
 * @author Luis
 */
public class AsciiABinario{    
    private ValorDeLetra[] tiposDeLetras;
    private String cadena;
    
    public AsciiABinario(String cadena){       
        tiposDeLetras = new ValorDeLetra[100];
        this.cadena = cadena;
        
    }

    public String convertidorDeAsciiABinario(String texto){
        String numBinario = "";
        String nombreDelArchivo = "caracteres";   
        llenarArreglos(nombreDelArchivo, tiposDeLetras);          
        for(int x =0; x < texto.length(); x++){
            String letra = texto.substring(x,x+1);
            loop:
          for(ValorDeLetra le: tiposDeLetras){
                if(le != null){
                    if(letra.equals(le.getLetra())){
                        int valorASCII = le.getValor();
                        numBinario += letra+" = "+ convercionDecimalABinario(valorASCII+"")+"\n";
                        break loop;
                    }
                }
            }
            
        }
        return numBinario;
        
    }
    
    
    public void llenarArreglos(String nombreArchivo, ValorDeLetra[] arre){
        System.out.println("Dentro de llenar Arreglos");
         try{       
            FileInputStream archivo = new FileInputStream("c://"+nombreArchivo+".txt");         
            DataInputStream entrada = new DataInputStream(archivo);       
            BufferedReader br = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;      
            int n = 0;          
            while ((strLinea = br.readLine()) != null){ 
                
                StringTokenizer token = new StringTokenizer(strLinea,",");
                String letra = "";
                String valor = "";
                int valorLetra = 0;
             try{
                  while(token.hasMoreTokens()){                     
                   letra = token.nextToken();                      
                   valor = token.nextToken();                  
                   valorLetra = Integer.parseInt(valor);                     
                   ValorDeLetra le = new ValorDeLetra(letra, valorLetra);                                      
                     arre[n] = le;                    
                     n++;
                }
             }catch(Exception ee){        
             }   
            }
           entrada.close();
        }catch (Exception e){ 
        }
    }
   
     public String convercionDecimalABinario(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numBinario = new String[100];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 2;                
                          decimal = decimal /2;                
                    numBinario[x] = residuo+"";
                    x++;
                                  
              }while(decimal > 0);               
             for(int y = numBinario.length-1; y >= 0; y--){            
                if(!(numBinario[y] == null)){
                  resultado += numBinario[y];
                }
        }
        return resultado;
    }
    
     public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    public static void main(String[] args){
        AsciiABinario convertidor = new AsciiABinario("Luis Enrique Silva Martinez");
       
        System.out.println("El Texto ingresado = "+convertidor.getCadena()
                          +"\nConvertido a binario = "
                          +"\n"+convertidor.convertidorDeAsciiABinario(convertidor.getCadena()));
    }
    
}
